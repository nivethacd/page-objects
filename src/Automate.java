import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Automate {
    public static WebDriver driver;

    public static void main(String args[]) {
        // Setting browser driver
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        // Navigating to Base URL
        driver.get("http://automationpractice.com/index.php");

        Home home = new Home(driver);
        home.clickSignIn();

        SignIn signin = new SignIn(driver);
        signin.enterEmail("nivetha467@gmail.com");
        signin.clickSubmit();

        Register register = new Register(driver);
        register.clickGender();
        register.enterCustomerFirstname("Nivetha");
        register.enterCustomerLastname("Clementina");
        register.enterPasswd("nivetha");
        register.enterFirstname("Nivetha");
        register.enterLastname("Clementina");
        register.enterCompany("TestVagrant");
        register.enterAddress("Bengaluru, India");
        register.enterCity("Bangalore");
        register.selectState("3");
        register.enterPostcode("56312");
        register.selectCountry("United States");
        register.enterPhone("1234567890");
        register.enterAlias("Home");
        register.clickSubmit();

        Category category = new Category(driver);
        category.searchQueryBox("Top");
        category.clickImage();
        System.out.println(category.getItemName());
        category.clickAddCart();

        //Quit browser
        driver.quit();

    }
}
