import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Category extends pageInitialisation{
    public Category(WebDriver driver){
        super(driver);
    }

    @FindBy(id = "search_query_top")
    WebElement searchBox;

    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li/div/div[1]/div/a[1]/img")
    WebElement image;

    @FindBy(xpath = "//*[@id=\"center_column\"]/div/div/div[3]/h1")
    WebElement itemName;

    @FindBy(xpath = "//*[@id=\"add_to_cart\"]/button")
    WebElement addCart;

    public void searchQueryBox(String str){
        searchBox.sendKeys(str+ Keys.RETURN);
    }

    public void clickImage(){
        image.click();
    }

    public String getItemName(){
        return itemName.getText();
    }

    public void clickAddCart(){
        addCart.click();
    }
}


