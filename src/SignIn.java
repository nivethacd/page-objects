import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignIn extends pageInitialisation{
    public SignIn(WebDriver driver){
        super(driver);
    }

    @FindBy(id = "email_create")
    WebElement email;

    @FindBy(id = "SubmitCreate")
            WebElement submitbtn;

    public void enterEmail(String str){
        email.sendKeys(str);
    }

    public void clickSubmit(){
        submitbtn.click();
    }
}
